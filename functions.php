<?php
/**
 * scrum functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package scrum
 */

if ( ! function_exists( 'scrum_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function scrum_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on scrum, use a find and replace
		 * to change 'scrum' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'scrum', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'scrum' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'scrum_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'scrum_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function scrum_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'scrum_content_width', 640 );
}
add_action( 'after_setup_theme', 'scrum_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function scrum_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'scrum' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'scrum' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'scrum_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function scrum_scripts() {
	wp_enqueue_style( 'scrum-style', get_stylesheet_uri() );

	wp_enqueue_style( 'scrum-foundation', get_template_directory_uri() . '/css/foundation.min.css');


	wp_enqueue_script( 'scrum-jquery', get_template_directory_uri() . '/js/jquery.js');
	wp_enqueue_script( 'scrum-app-js', get_template_directory_uri() . '/js/app.js', array('scrum-jquery', 'scrum-foundation-js'));
	wp_enqueue_script( 'scrum-foundation-js', get_template_directory_uri() . '/js/foundation.min.js', array('scrum-jquery'), '20151215', true );

	wp_enqueue_script( 'scrum-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'scrum-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'scrum_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Teams
 */
require get_template_directory() . '/inc/post-type-teams.php';

/**
* Projects
*/
require get_template_directory() . '/inc/post-type-projects.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

/**
 * Add options page for asana information.
 */
 if( function_exists('acf_add_options_page') ) {
 		acf_add_options_page();
 }

 /**
  * API Endpoints
  */
 require get_template_directory() . '/inc/api/endpoints.php';

 /**
  * Authentication logic for Asana
  */
 require get_template_directory() . '/inc/api/asana/oauth.php';

 /**
	* Handles all of the logic for the Asana integration with the exception of authentication which is handled by the wordpress api
	*/
 require get_template_directory() . '/inc/asana/AsanaIntegration.php';

 /**
	* Everhour integration
	*/
 require get_template_directory() . '/inc/everhour/EverhourIntegration.php';

 /**
	* Handles cacheing task data using wordpresses transient feature.
	*/
 require get_template_directory() . '/inc/transients.php';

 /**
 * Data describing a task (includes information from Asana and Everhour)
 */
 require get_template_directory() . '/inc/task.php';

 /**
 * Data describing a project
 */
 require get_template_directory() . '/inc/project.php';

 /**
 * List of tasks
 */
 require get_template_directory() . '/inc/task-list.php';

 /**
 * Handles bulk actions like synching asana projects
 */
 require get_template_directory() . '/inc/api/asana/bulk-actions.php';

 /**
 	* Add custom logo to login page
	*/
	function jsix_login_logo() { ?>
	    <style type="text/css">
	        #login h1 a, .login h1 a {
	            background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/jumpsix-logo-triangle.png);
							height: 80px;
					    width: 80px;
					    background-size: contain;
					    background-repeat: no-repeat;
					    padding-bottom: 0px;
	        }
	    </style>
	<?php }
	add_action( 'login_enqueue_scripts', 'jsix_login_logo' );


	/**
	 * Don't force users to be logged in when accessing the API.
	 */
	 remove_filter( 'rest_authentication_errors', 'v_forcelogin_rest_access', 99 );
