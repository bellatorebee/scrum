<?php

class TransientManager
{
    private $user_id;

    function __construct($user_id) {
        $this->user_id = $user_id;
    }

    function check_list($list_name) {
        if(get_transient($list_name . $this->user_id)) {
            return true;
        }
        else {
            return false;
        }
    }

    //Loads tasks from transient.
    function get_list($list_name) {
        return get_transient($list_name . $this->user_id);
    }

    //Saves tasks as a transient.
    function save_list($list_name, $data) {
        switch($list_name)
        {
            case "tasks_today":
                set_transient($list_name . $this->user_id, $data, 60*10 );
                break;
            case "tasks_yesterdays_activity":
                set_transient($list_name . $this->user_id, $data, $this->seconds_untill_tommorow());
                break;
            default:
                set_transient($list_name . $this->user_id, $data, 60*10 );
                break;
        }
    }

    function seconds_untill_tommorow() {
        return strtotime('tomorrow') - time();
    }
}
