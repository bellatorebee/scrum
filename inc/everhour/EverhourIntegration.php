<?php

class EverhourIntegration {
  private $api_key;
  private $asana_tasks;
  private $user_id;
  private $aggregate_time;

  function __construct($user_id, $asana_id) {
      $this->api_key = get_field('everhour_api_key', 'options');
      $this->user_id = $user_id;

      /*
       * Load the users Asana tasks. This is being done once at initialization to prevent an API query to asana per task.
       * In instances where the task isn't loaded because it's positioned down too far in the users list, it will be queried by id.
       */

      $asana = new AsanaIntegration();
      $this->asana_tasks = $asana->load_user_tasks($asana_id);
  }

  function get_aggregate_time() {
      return $this->aggregate_time;
  }

  //Gets all of the time entries for a user that occured the previous day.
  function get_yesterdays_activity_log() {
      $url = "https://api.everhour.com/users/" . $this->user_id . "/time?limit=100&offset=0";

      $response = wp_remote_get($url, array(
            'headers' => 'X-Api-Key: ' . $this->api_key,
            'timeout' => 5000
      ));

      if(! is_wp_error($response)) {
          $time_entries = json_decode($response["body"]);
          $yesterdays_entries = array();

          $three_day_force = isset( $_GET['friday'] );
          if ( 'Mon' == date( 'D', time() ) || $three_day_force ) {
              $yesterday = date( "Y-m-d", strtotime( 'last Friday' ) );
          } else {
              $yesterday = date("Y-m-d", strtotime( '-1 days' ) );
          }
          
          foreach($time_entries as $time_entry) {
              if($time_entry->date == $yesterday) {
                  $yesterdays_entries[] = $time_entry;
              }
          }

          return $this->parse_task_data($yesterdays_entries);
      }
      else {
          return "There was an error in the everhour API.";
      }
  }

  function parse_task_data($yesterdays_entries) {
      $tasks = array();
      $aggregate_time = 0;

      foreach($yesterdays_entries as $entry) {
            $aggregate_time += $entry->time;

            //Add all of the everhour data to a Task object.
            $task = new Task();
            $task->setName($entry->task->name);

            $seconds = $entry->time;
            $task->setTime($this->calculate_time_string($seconds));

            $task->setId(substr($entry->task->id, 3));

            if(isset($entry->task->parentId)) {
                $task->setParentId($entry->task->parentId);
            }

            if(isset($entry->task->parentName)) {
                $task->setParentName($entry->task->parentName);
            }

            //Add projects
            if(isset($entry->task->projects)) {
                $projects = array();

                foreach($entry->task->projects as $everhour_project)
                {
                    $asana_project_id = substr($everhour_project, 3);

                    //Query for the project stored in the WP db.
                    $args = array(
                    	'numberposts'	=> -1,
                    	'post_type'		=> 'project',
                    	'meta_key'		=> 'project_asana_id',
                    	'meta_value'	=> $asana_project_id
                    );

                    $query = new WP_Query($args);

                    if($query->have_posts())
                    {
                        while ($query->have_posts())
                        {
                            $query->the_post();

                            //Save the data to a project object.
                            $project = new Project();
                            $project->setId($asana_project_id);
                            $project->setName(get_the_title());

                            $projects[] = $project;
                        }
                    }
                    else
                    {
                        // Load from Asana.
                        $asana = new AsanaIntegration();
                        $asana_project = $asana->get_project($asana_project_id);

                        // Save as project in WordPress for future use.
                        $tmp_project = array(
                          'post_title'    => $asana_project->name,
                          'post_type'     => 'project',
                          'post_status'   => 'publish',
                          'post_author'   => 1
                        );

                        $project_wp_id = wp_insert_post($tmp_project);
                        update_field('project_asana_id', $asana_project->id, $project_wp_id);

                        // Add to projects array.
                        $project = new Project();
                        $project->setId($asana_project_id);
                        $project->setName($asana_project->name);

                        $projects[] = $project;
                    }
                }

                wp_reset_postdata();
                $task->setProjects($projects);
            }



            //$task->setProjects();


            //Load the task from Asana to build out the remaining data.
            //$asana_task = $this->load_asana_data($task->getId());
            //$task->setCurrentStatus();
            //$task->setPriority();

            $tasks[] = $task;
      }
      $this->aggregate_time = $this->calculate_time_string($aggregate_time);

      return $tasks;
  }

  //Check to see if task exists in the list of tasks we already queried. If it doesn't, load the task by ID from Asana.
  function load_asana_data($task_id) {
      foreach($this->asana_tasks as $task) {
          if($asana->id == $task_id) {
              return $task;
          }
      }
      $asana = new AsanaIntegration();
      return $asana->get_task($task_id);
  }

  function calculate_time_string($seconds) {
      $hours = floor($seconds / 3600);
      $minutes = floor(($seconds % 3600) / 60);

      if($hours) {
          $time_string = $hours . "h";
      }
      if($minutes) {
          if($hours) {
              $time_string .=  ' ' . $minutes . "m";
          }
          else {
              $time_string = $minutes . "m";
          }
      }

      if(isset($time_string)) {
          return $time_string;
      }
      else {
          return '0h 0m';
      }
  }

  function yesterdays_time($user_id) {
      $url = "https://api.everhour.com/users/" . $user_id . "/time?limit=100&offset=0";

      $response = wp_remote_get($url, array(
            'headers' => 'X-Api-Key: ' . $this->api_key,
            'timeout' => 5000
      ));

      if(! is_wp_error($response)) {
          $time_entries = json_decode($response["body"]);

          $aggregate_time = 0;

          $yesterday = date("Y-m-d", strtotime( '-1 days' ) );
          foreach($time_entries as $time_entry) {
              if($time_entry->date == $yesterday) {
                  $aggregate_time += $time_entry->time;
              }
          }

          $hours = floor($aggregate_time / 3600);
          $minutes = floor(($aggregate_time % 3600) / 60);

          if($hours) {
              $time_string = $hours . "h";
          }
          if($minutes) {
              if($hours) {
                  $time_string .=  ' ' . $minutes . "m";
              }
              else {
                  $time_string = $minutes . "m";
              }
          }

          if(isset($time_string)) {
              return $time_string;
          }
          else {
              return '0h 0m';
          }
      }
      else {
          return "There was an error in the everhour API.";
      }
  }

}
