<?php

class AsanaIntegration {
    var $access_token;
    var $base_url = "https://app.asana.com/api/1.0";
    var $workspace = "246829547287634";
    var $api_attempts = 0;
    var $total_tasks;

    function __construct() {
        $this->access_token = get_field('access_token', 'options');
    }

    function get_total_tasks() {
        return $this->total_tasks;
    }

    function get_all_projects() {
        $url = $this->base_url . '/projects?workspace=246829547287634';

        $result = $this->send_get($url);

        if(! is_wp_error($result)) {
            return $result->data;
        }
        else {
            return null;
        }
    }

    function get_project($project_id) {
        $url = $this->base_url . '/projects/' . $project_id;

        $result = $this->send_get($url);

        if(! is_wp_error($result)) {
            return $result->data;
        }
        else {
            return null;
        }
    }

    function get_task($asana_task_id) {
        $url = $this->base_url . '/tasks/' . $asana_task_id;
        $url .= "?opt_field=parent,completed";
        $url .= '&opt_expand=parent,completed_at,tags,projects,completed,memberships,assignee_status';
        $url .= "&workspace=246829547287634";

        $result = $this->send_get($url);

        if(! is_wp_error($result)) {
            return $this->format_tasks($result->data);
        }
        else {
            return null;
        }
    }

    function load_user_tasks($asana_user_id) {
        $url = $this->base_url . '/tasks';
        $url .= "?opt_field=parent,completed";
        $url .= '&opt_expand=parent,completed_at,tags,projects,completed,memberships,assignee_status';
        $url .= "&assignee=" . $asana_user_id;
        $url .= "&workspace=246829547287634";
        $url .= "&limit=100";

        $result = $this->send_get($url);

        if(! is_wp_error($result)) {
            return $this->format_tasks($result->data);
        }
        else {
            return null;
        }
    }

    function get_todays_user_tasks($asana_user_id) {
        $url = $this->base_url . '/tasks';
        $url .= "?opt_field=completed_since,parent,completed";
        $url .= '&opt_expand=parent,completed_at,tags,projects,completed,memberships,assignee_status';
        $url .= "&assignee=" . $asana_user_id;
        $url .= "&workspace=246829547287634";
        $url .= "&completed_since=now";
        $url .= "&limit=100";

        $result = $this->send_get($url);

        if(! is_wp_error($result)) {
            $this->total_tasks = count($result->data);
            return $this->format_tasks($result->data);
        }
        else {
            return null;
        }
    }

    function format_tasks($tasks) {
        $todays_tasks = [];
        foreach($tasks as $task) {
            if($task->assignee_status == "today") {
                $tmp_task = new Task();
                $tmp_task->setId($task->id);
                $tmp_task->setName($task->name);
                $tmp_task->setParentName($task->parent->name);
                $tmp_task->setParentId($task->parent->id);

                //Check to see if the sub task has a project. If it doesn't, assign it to it's parents.
                if(! $task->projects) {
                    $tmp_task->setProjects($task->parent->projects);
                }
                else {
                    $tmp_task->setProjects($task->projects);
                }

                $tmp_task->setParentAssigneeId($task->parent->assignee->id);

                if(isset($task->tags)) {
                    foreach($task->tags as $tag) {
                        if($tag->name == 'High Priority') {
                            $tmp_task->setPriority('High Priority');
                        }
                        else if($tag->name == 'Medium Priority') {
                            $tmp_task->setPriority('Medium Priority');
                        }
                        else if($tag->name == 'Low Priority') {
                            $tmp_task->setPriority('Low Priority');
                        }
                    }
                }

                $todays_tasks[] = $tmp_task;
            }
        }

        return $todays_tasks;
    }

    function send_get($url) {
        $response = wp_remote_get($url, array(
              'headers' => 'Authorization: Bearer ' . $this->access_token,
              'timeout' => 15
        ));

        //echo '<pre>';
        //var_dump($response);
        //echo '</pre>';

        //If access token is expired, refresh it.
        if(! is_wp_error($response) && $response["response"]["code"] == '401') {

            //Trigger endpoint that refreshes the access token.
            $api_response = wp_remote_get(site_url() . '/wp-json/jsix/v1/refresh-expired-token');

            $access_token = json_decode($api_response["body"]);

            //Resend request.
            $response = wp_remote_get($url, array(
                'headers' => 'Authorization: Bearer ' . $access_token
            ));
        }
        if(! is_wp_error($response)) {
            return json_decode($response["body"]);
        }
        else {
            return $response;
        }
    }
}
