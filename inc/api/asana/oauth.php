<?php

// https://asana.com/developers/documentation/getting-started/auth#register-an-app

function jsix_authorize_user() {
    /*
      https://app.asana.com/-/oauth_authorize?
      client_id=123&
      redirect_uri=https://myapp.com/oauth
      &response_type=code
      &state=somerandomstate
    */

    // Check if user is logged in and an admin. If they are, redirect user to the asana authorization page.
    $url = "https://app.asana.com/-/oauth_authorize";
    $url .= "?client_id=" . get_field('client_id', 'options');
    $url .= "&redirect_uri=" . get_field('redirect_uri', 'options');
    $url .= "&response_type=code";
    $url .= "&state=" . random_alphanumeric_string();

    if ( wp_redirect( $url ) ) {
        exit;
    }
}

function jsix_redirect_handle(WP_REST_Request $request) {
    /*
      https://myapp.com/oauth?
      code=0%2F817a776a358a5d8d89988562d2b3dc8f&
      state=somerandomstate
    */
    $code = $request['code'];
    $state = $request['state'];

    $url = "https://app.asana.com/-/oauth_token";

    $response = wp_remote_post( $url, array(
        'method'            => 'POST',
        'timeout'           => 5,
        'headers'           => array(),
        'body'              => array(
            'grant_type'    => 'authorization_code',
            'client_id'     => '720615952560692',
            'client_secret' => '9771d9b89630580f2d5d29c92f87088a',
            'redirect_uri'  => 'https://teams.jumpsixmarketing.com/wp-json/jsix/v1/redirect',
            'code'          => $code
        )
        )
    );

    $body = json_decode($response["body"]);

    update_field('refresh_token', $body->refresh_token, 'options');
    update_field('access_token', $body->access_token, 'options');

    // Check to see if user granted access. If they did, trigger the logic to create a refresh token.

    // If they didn't, output an appropriate error.
}

function jsix_refresh_expired_token() {
  /*
    https://app.asana.com/-/oauth_token

    Generate a new access token using the refresh token.
  */

  $url = "https://app.asana.com/-/oauth_token";
  $refresh_token = get_field('refresh_token', 'options');

  $response = wp_remote_post( $url, array(
      'method'      => 'POST',
      'timeout'     => 5000,
      'headers'     => array(),
      'body'        => array(
          'grant_type' => 'refresh_token',
          'client_id' => '720615952560692',
          'client_secret' => '9771d9b89630580f2d5d29c92f87088a',
          'redirect_uri' => 'https://teams.jumpsixmarketing.com/wp-json/jsix/v1/asana-authorize',
          'refresh_token' => $refresh_token
      )
      )
  );
  if(! is_wp_error($response)) {
      $decoded_response = json_decode($response["body"]);
      update_field('access_token', $decoded_response->access_token, 'options');

      return $decoded_response->access_token;
  }
  else {
    var_dump($response);
  }
}

function random_alphanumeric_string($length = 12) {
    $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    return substr(str_shuffle($chars), 0, $length);
}
