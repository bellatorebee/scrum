<?php

add_action( 'rest_api_init', function () {

    //http://scrum.local/wp-json/jsix/v1/asana-authorize
    register_rest_route( 'jsix/v1', '/asana-authorize', array(
        'methods' => 'GET',
        'callback' => 'jsix_authorize_user',
    ));

    //https://teams.jumpsixmarketing.com/wp-json/jsix/v1/redirect?code=0%2Fa3839b9d62f67d73e5a9019557b5b9d3&state=WsFRdM1Y7tzI
    register_rest_route( 'jsix/v1', '/redirect', array(
        'methods' => 'GET',
        'callback' => 'jsix_redirect_handle',
    ));

    register_rest_route( 'jsix/v1', '/generate-refresh-token', array(
        'methods' => 'GET',
        'callback' => 'jsix_generate_refresh_token',
    ));

    //http://scrum.local/wp-json/jsix/v1/refresh-expired-token
    register_rest_route( 'jsix/v1', '/refresh-expired-token', array(
        'methods' => 'GET',
        'callback' => 'jsix_refresh_expired_token',
    ));

    //http://scrum.local/wp-json/jsix/v1/synch-asana-projects
    register_rest_route( 'jsix/v1', '/synch-asana-projects', array(
        'methods' => 'GET',
        'callback' => 'jsix_synch_asana_projects',
    ));

});
