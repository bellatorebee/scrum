
<section>
          <ul class="dropdown menu" data-dropdown-menu>
              <li class="team-dropdown">
                  <a><?php the_title(); ?></a>
                  <ul class="menu">
                      <?php
                          $args = array (
                            'post_type'              => array( 'team' ),
                            'post_status'            => array( 'publish' ),
                            'nopaging'               => true,
                            'order'                  => 'ASC',
                            'orderby'                => 'menu_order',
                          );

                          $teams = new WP_Query( $args );
                          if ( $teams->have_posts() )
                          {
                              while ($teams->have_posts())
                              {
                                  $teams->the_post();

                                  ?>
                                  <li class="is-submenu-item is-dropdown-submenu-item"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></li>
                                  <?php
                              }
                          }

                          wp_reset_postdata();
                      ?>
                  </ul>
              </li>
          </ul>
  </section>
