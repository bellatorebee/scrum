<div class="grid-x grid-padding-x print-wrapper notes-wrapper" style="display:none;">
    <div class="note-column">
        <div class="notes-headline"><?php echo $previous_work_day; ?></div>
    </div>
    <div class="note-column">
        <div class="notes-headline">Today</div>
    </div>
    <div class="vertical-notes-divider"></div>
<?php
    if(have_rows('team_members')):
        $team_num = count(get_field('team_members'));
        $rows = floor(20/ $team_num);

    		while ( have_rows('team_members') ) : the_row();
    				$user = get_sub_field('team_members');

            echo '<div class="notes-title">' . $user->nickname . '</div>';

            for ($x = 0; $x <= $rows; $x++) :
                echo '<div class="note-line"></div>';
            endfor;

        endwhile;
    endif;

echo '</div>';
