<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package scrum
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<?php scrum_post_thumbnail(); ?>

<div class="entry-content">
<section>
<div class="user-outside-wrapper">

<?php

$three_day_force = isset( $_GET['friday'] );
if ( 'Mon' == date( 'D', time() ) || $three_day_force ) {
		$previous_work_day = 'Friday';
		global $wpdb;
		$sql = 'DELETE FROM ' . $wpdb->options . ' WHERE option_name LIKE "_transient_%"';
    $wpdb->query($sql);
		wp_cache_flush();
} else {
		$previous_work_day = 'Yesterday';
}

if(have_rows('team_members')):
		while ( have_rows('team_members') ) : the_row();
				$user = get_sub_field('team_members');

				//Asana Data
				$asana_id = get_field('asana_id', 'user_' . $user->ID);
				if($asana_id) {
						$task_list = new TaskList('tasks_today', $asana_id, $user->ID);
						$todays_tasks = $task_list->get_today();
						$total_tasks = $task_list->get_total_tasks();
				}

				//Everhour Data
				$everhour_id = get_field('everhour_id', 'user_' . $user->ID);
				if($everhour_id && $asana_id) {
						$task_list = new TaskList('tasks_yesterdays_activity',  $asana_id, $user->ID);
						$task_list->set_everhour_id($everhour_id);
						$activity_log = $task_list->get_activity_log();
						$total_time = $task_list->get_aggregate_time();
				}
				else {
						$time_string = "Unknown";
				}

				//Profile Picture
				$profile_photo_attachment_id = get_field('profile_picture', 'user_' . $user->ID);
				$profile_photo = wp_get_attachment_image_src($profile_photo_attachment_id, 'thumbnail');

								if($asana_id):
										?>
										<div class="grid-x grid-padding-x print-wrapper">
										<div class="cell small-12 medium-12 large-4 profile-box">
												<div class="profile-container container">
														<div class="profile-top">
																<a href="<?php the_field('my_tasks', 'user_' . $user->ID); ?>" target="_blank">
																		<div class="profile-pic" style="background-image:url('<?php echo $profile_photo[0]; ?>');"></div>
																		<div class="profile-name"><?php echo $user->nickname; ?></div>
																</a>
														</div>
														<hr>
														<div class="line-item">
																<div class="line-item-title">Total Tasks</div>
																<div class="line-item-value"><?php echo $total_tasks; ?></div>
														</div>
														<div class="inside-divider">
																<hr class="inside-divider">
														</div>
														<div class="line-item">
																<div class="line-item-title"><?php echo $previous_work_day; ?>s Time</div>
																<div class="line-item-value"><?php echo $total_time; ?></div>
														</div>
														<div class="inside-divider">
																<hr>
														</div>
												</div>
										</div>
										<div class="cell small-12 medium-12 large-8 task-top-container">
												<div class="cell small-12 task-column-container">
														<div class="task-title">Today</div>
														<div class="task-container container">
																<?php
																if($todays_tasks):
																		foreach($todays_tasks as $task):
																				echo '<div class="task">';
																						//Priority dot
																						echo $task->getPriorityDotHtml();

																						//Task name
																						if($task->getType() == "child") {
																								echo '<span class="task-name"><span class="sub-task-icon">&raquo;</span> ' . $task->getName() . '<span class="print-show title-letter"> - ' . $task->getPriorityFirstLetter() . '</span></span>';
																						}
																						else {
																								echo '<span class="task-name">' . $task->getName() . '<span class="print-show title-letter"> - ' . $task->getPriorityFirstLetter() . '</span></span>';
																						}

																						//Project name
																						if($task->getProjects() && $task->getType() != "child") {
																								$projects = $task->getProjects();
																								$project = $projects[0];
																								echo '<span class="project-name pill">' . $project->name . '</span>';
																						}
																				echo '</div>';
																		endforeach;
																else:
																		echo '<span class="no-tasks">0 Tasks</span>';
																endif;
																?>
														</div>
												</div>
												<div class="cell small-12 task-column-container">
														<div class="task-title"><?php echo $previous_work_day; ?>s Activity</div>
														<div class="task-container container activity-log">
															<?php
																if($activity_log):
																		foreach($activity_log as $log_item):
																				echo '<div class="task">';
																						//Task time
																						echo '<span class="time">' . $log_item->getTime() . '</span>';

																						//Task name
																						echo '<span class="task-name">' . $log_item->getName() . '</span>';

																						//Project name
																						if($log_item->getProjects() && $log_item->getType() != "child") {
																								$projects = $log_item->getProjects();
																								$project = $projects[0];
																								echo '<span class="project-name pill">' . $project->getName() . '</span>';
																						}
																				echo '</div>';
																		endforeach;
																else:
																		echo '<span class="no-tasks">No time was tracked.</span>';
																endif;
															?>
														</div>
												</div>
										</div>
										</div>
										<?php
								endif;
		endwhile;
endif;
?>
<?php get_template_part( 'template-parts/components/notes'); ?>
</div>
</div>
</section>

	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->
