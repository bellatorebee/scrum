<?php

class TaskList
{
    private $list_name;

    private $asana_id;
    private $everhour_id;
    private $user_id;
    private $aggregate_time;
    private $total_tasks;

    function __construct($list_name, $asana_id, $user_id) {
        $this->list_name = $list_name;
        $this->asana_id = $asana_id;
        $this->user_id = $user_id;
    }

    //Sets the everhour ID, this is done outside of the construct function because some users may not have one.
    function set_everhour_id($everhour_id) {
        $this->everhour_id = $everhour_id;
    }

    //Gets all of the tasks a user has marked for today.
    function get_today() {
        $transients = new TransientManager($this->user_id);

        if(! $transients->check_list('tasks_today')) {
            //Pull in todays tasks from asana
            $asana = new AsanaIntegration();
            $tasks = $asana->get_todays_user_tasks($this->asana_id);

            //Restructure task data
            $tasks = $this->structure_tasks($tasks);

            //Save task data to transient
            $transients->save_list('tasks_today', $tasks);

            //Save total tasks to transient
            $transients->save_list('value_total_tasks', $asana->get_total_tasks());

            //Save value to self as a fallback, primarily for testing.
            $this->total_tasks = $asana->get_total_tasks();

            //Return todays tasks
            return $tasks;
        }
        else {
            return $transients->get_list('tasks_today');
        }
    }

    //Gets the aggregate time for the previous day. get_activity_log() must be called first.
    function get_aggregate_time() {
        $transients = new TransientManager($this->user_id);

        if(! $transients->check_list('value_aggregate_time')) {
        //if(true) {
            return $this->aggregate_time;
        }
        else {
            return $transients->get_list('value_aggregate_time');
        }
    }

    //Gets the total tasks a user has assigned. get_activity_log() must be called first.
    function get_total_tasks() {
        $transients = new TransientManager($this->user_id);

        if(! $transients->check_list('value_total_tasks')) {
        //if(true) {
            return $this->total_tasks;
        }
        else {
            return $transients->get_list('value_total_tasks');
        }
    }

    //Gets all of the tasks the user tracked time against yesterday.
    function get_activity_log() {
        $transients = new TransientManager($this->user_id);

        if(! $transients->check_list('tasks_yesterdays_activity')) {
        //if(true) {
            //Pull in yesterdays tasks
            $everhour = new EverhourIntegration($this->everhour_id, $this->asana_id);
            $tasks = $everhour->get_yesterdays_activity_log();

            //Restructure task data
            $tasks = $this->structure_tasks($tasks);

            //Save task data to transient
            $transients->save_list('tasks_yesterdays_activity', $tasks);

            if(! empty($everhour->get_aggregate_time()))
            {
                //Save aggregate time to transient
                $transients->save_list('value_aggregate_time', $everhour->get_aggregate_time());

                //Save value to self as a fallback, primarily for testing.
                $this->aggregate_time = $everhour->get_aggregate_time();
            }

            //Return activity log
            return $tasks;
        }
        else {
            return $transients->get_list('tasks_yesterdays_activity');
        }
    }

    //Accepts a list of tasks, and returns flattened tree structure where tasks have hiearchy, and an appropriate hierarchy label (type).
    private function structure_tasks($tasks) {

        foreach($tasks as $task)
        {
            if($task->getParentId() == null) {
                $task->setType("parent");
            }
            else {
                if($task->getParentAssigneeId() == $this->asana_id) {
                    //Even if the parent task is assigned, we don't want to show this as a subtask unless the parent task is a part of this specific task list.
                    if($this->task_in_list($tasks, $task->getParentId())) {
                        $task->setType("child");
                    }
                }
                else {
                    $task->setType("sub-without-parent");
                }
            }
        }

        return $this->sort_tasks($tasks);
    }

    //Checks to see if task id exists in a list of tasks_today
    private function task_in_list($tasks, $task_id) {
        foreach($tasks as $task) {
            if($task->getId() == $task_id) {
                return true;
            }
        }
        return false;
    }

    //Sorts tasks into a tree structure.
    private function sort_tasks($tasks)
    {
        $tmp_array = array();

        //Loop through all of the tasks.
        foreach($tasks as $task)
        {
            if(empty($task->getParentId())) {
                $tmp_array[] = $task;
                $children = $this->get_child_tasks($tasks, $task->getId());
                $tmp_array = array_merge($tmp_array, $children);
            }
            else {
                if(! $this->has_parent($tasks, $task->getParentId())) {
                    $tmp_array[] = $task;
                }
            }
        }

        return $tmp_array;
    }

    //Checks to see if we have the parent task assigned.
    function has_parent($tasks, $parent_id) {
        foreach($tasks as $task)
        {
            if($task->getId() == $parent_id) {
                return true;
            }
        }
        return false;
    }

    //Gets all of the child tasks.
    function get_child_tasks($tasks, $parent_id)
    {
        $children = array();

        foreach($tasks as $task)
        {
            //If task has a parent, and it's id matches the parent we're trying to find, add it.
            if($task->getParentId() && ($task->getParentId() == $parent_id))
            {
                $children[] = $task;
            }
        }

        return $children;
    }

    //Filter out all of the tasks that are not set to today. As of 7.13.2018 this function does not appear to be needed.
    function remove_todays_tasks($tasks) {
        $filtered_tasks = [];

        foreach($tasks as $task)
        {
            if($task->assignee_status != "today") {
                $filtered_tasks[] = $task;
            }
        }

        return $filtered_tasks;
    }
}
