<?php

class Task
{
    private $id;
    private $name;
    private $parent_name;
    private $parent_id;
    private $projects;
    private $priority;
    private $time;
    private $type;
    private $parent_assignee_id;


    /**
     * Get the value of Id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set the value of Id
     *
     * @param mixed id
     *
     * @return self
     */
    public function setId($id)
    {
        $this->id = preg_replace("/[^0-9]/", "", $id);

        return $this;
    }

    /**
     * Get the value of Name
     *
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set the value of Name
     *
     * @param mixed name
     *
     * @return self
     */
    public function setName($name)
    {
        if(strlen($name) > 30) {
            $this->name = substr($name, 0, 27) . '...';
        }
        else {
            $this->name = $name;
        }

        return $this;
    }

    /**
     * Get the value of Parent Name
     *
     * @return mixed
     */
    public function getParentName()
    {
        return $this->parent_name;
    }

    /**
     * Set the value of Parent Name
     *
     * @param mixed parent_name
     *
     * @return self
     */
    public function setParentName($parent_name)
    {
        $this->parent_name = $parent_name;

        return $this;
    }

    /**
     * Get the value of Parent Id
     *
     * @return mixed
     */
    public function getParentId()
    {
        return $this->parent_id;
    }

    /**
     * Set the value of Parent Id
     *
     * @param mixed parent_id
     *
     * @return self
     */
    public function setParentId($parent_id)
    {
        $this->parent_id = $parent_id;

        return $this;
    }

    /**
     * Get the value of Priority
     *
     * @return mixed
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * Get the html markup of Priority dot
     *
     * @return mixed
     */
    public function getPriorityDotHtml()
    {
        if(! empty($this->priority)) {
            if($this->priority == 'High Priority') {
                return '<span class="dot high-priority"></span>';
            }
            else if($this->priority == 'Medium Priority') {
                return '<span class="dot medium-priority"></span>';
            }
            else if($this->priority == 'Low Priority') {
                return '<span class="dot low-priority"></span>';
            }
        }
        else {

        }
    }

    public function getPriorityFirstLetter()
    {
        if(! empty($this->priority)) {
            if($this->priority == 'High Priority') {
                return 'H';
            }
            else if($this->priority == 'Medium Priority') {
                return 'M';
            }
            else if($this->priority == 'Low Priority') {
                return 'L';
            }
        }
        else {

        }
    }
    /**
     * Set the value of Priority
     *
     * @param mixed priority
     *
     * @return self
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get the value of Time
     *
     * @return mixed
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set the value of Time
     *
     * @param mixed time
     *
     * @return self
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get the value of Projects
     *
     * @return mixed
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * Set the value of Projects
     *
     * @param mixed projects
     *
     * @return self
     */
    public function setProjects($projects)
    {
        $this->projects = $projects;

        return $this;
    }


    /**
     * Get the value of Type
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set the value of Type
     *
     * @param mixed type
     *
     * @return self
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }


    /**
     * Get the value of Parent Assignee Id
     *
     * @return mixed
     */
    public function getParentAssigneeId()
    {
        return $this->parent_assignee_id;
    }

    /**
     * Set the value of Parent Assignee Id
     *
     * @param mixed parent_assignee_id
     *
     * @return self
     */
    public function setParentAssigneeId($parent_assignee_id)
    {
        $this->parent_assignee_id = $parent_assignee_id;

        return $this;
    }

}
