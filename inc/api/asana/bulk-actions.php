<?php

function jsix_synch_asana_projects() {
    $asana = new AsanaIntegration();
    $projects = $asana->get_all_projects();

    if ( ! is_admin() ) {
        require_once( ABSPATH . 'wp-admin/includes/post.php' );
    }

    foreach($projects as $project)
    {
        if(! post_exists($project->name))
        {
            $tmp_project = array(
              'post_title'    => $project->name,
              'post_type'     => 'project',
              'post_status'   => 'publish',
              'post_author'   => 1
            );

            $project_wp_id = wp_insert_post($tmp_project);
            update_field('project_asana_id', $project->id, $project_wp_id);
        }
    }
}
